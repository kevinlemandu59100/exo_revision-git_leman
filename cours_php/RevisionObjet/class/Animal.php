<?php

//la classe c'est la structure / le moule de l'objet

class Animal {

    // Les Attributs sont des caractéristiques de l'objet
    // public private protected sont trois opérateur d'accecibilité
    
    private $id ;
    
    private $nom;

    private $couleur;

    // l'opérateur private rend l'attribut disponible dans la class ( le fichier )
    private $poids ;
    
    // La methode magique __construct permet de construire l'objet

    public function __construct( $id , $nom , $couleur, $poids )
    {
     
        // $this fait référence à l'objet courant ( celui que l'on manipule actuellement)
        $this->id = $id;        
        $this->nom = $nom;        
        $this->couleur = $couleur;        
        $this->poids = $poids;  
              
    }


    //accesseurs ( getter ) permet d'acceder à un attribut de l'objet

    /**
     * @return int l'attribut id de notre objet 
     */
    public function getId( ) : int {

        return $this->id;

    }

    /**
     * @return string l'attribut nom de l'objet 
     */
    public function getNom( ) : string {

        return $this->nom;

    }

    /**
     * @return string l'attribut couleur de l'objet 
     */
    public function getCouleur( ) : string {

        return $this->couleur;

    }
    

    /**
     * @return float l'attribut poids de l'objet 
     */
    public function getPoids( ) : float {

        return $this->poids;

    }


    //Les mutateurs setters  les méthodes qui permettent de modifier la valeur d'un attribut de l'objet 



    /**
     * @param $id la nouvelle valeur pour l'attribut id 
     */
    public function setId( $id ) {
        $this->id = $id;
    }
    
    /**
     * @param $nom la nouvelle valeur pour l'attribut id 
     */
    public function setNom( $nom ) {
        $this->nom = $nom;
    }

    /**
     * @param $couleur la nouvelle valeur pour l'attribut id 
     */
    public function setCouleur( $couleur ) {
        $this->couleur = $couleur;
    }

    /**
     * @param $id la nouvelle valeur pour l'attribut id 
     */
    public function setPoids( $poids ) {
        $this->poids = $poids;
    }


    public function __toString()
    {
        return
         " Id : "  .  $this->id . 
         " Nom : " . $this->nom . 
         " Couleur : " . $this->couleur .
         " Poids : " . $this->poids . " Kg "  ;      
        
        
    }

    
    /**
     * la methode affiche le nom l'animal et ce qu'il mange
     * @param @mixed $aliment l'aliment manger par l'animal 
     */
    public function manger(  $aliment ){

        echo $this->nom . "  mange " . $aliment;

    }

    



}



?>