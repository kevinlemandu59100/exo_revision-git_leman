<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animaux</title>
</head>

<body>

    <?php

    $listeDesPokemon = [

        'Pikachu' => [
            'Taille' => 0.4,
            'Poids' => 6,
        ],
        'Raichu' => [
            'Taille' => 0.8,
            'Poids' => 30,
        ],
        'Salameche' => [
            'Taille' => 1,
            'Poids' => 50,
        ],
        'Reptincel' => [
            'Taille' => 1.4,
            'Poids' => 60,
        ],
        'Dracaufeau' => [
            'Taille' => 2,
            'Poids' => 180,
        ],

    ];

    $age = 15;

    ?>


    <h1>Exemple d'imbrication du php dans l'html </h1>


    <?php if ($age <= 15) :  ?>

        <p>Bravo tu peux jouer ! </p>

    <?php endif ?>

    <ul>

        <?php foreach ($listeDesPokemon as $key => $value) : ?>

            <li> <?php echo $key ?> </li>

            <ul>
                <?php foreach ($value as $key2 => $valeur) : ?>
                <li> <?php echo $key2 ?> : <?php echo $valeur ?> </li>
                <?php endforeach ?>
            </ul>


        <?php endforeach ?>

    </ul>

    <?php if($age > 18  ): ?>
    <p> Tu es trop vieux pour jouer a ce jeux ! </p>
    <?php elseif($age == 18 ) : ?>

        <p>Bravo tu es majeur mais tu joue encore a des jeux pour enfants ! </p>

        <?php else : ?>

            <p>Bravo tu peux jouer !! Amuse toi bien ! </p>

            <?php endif ;

while($age == 5) :

    endwhile
?>
</body>

</html>