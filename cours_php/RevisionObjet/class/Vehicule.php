<?php

class Vehicule
{

    private $marque;
    private $couleur;
    private $prix;
    private $vitesse;

    public function __construct($marque, $couleur, $prix, $vitesse)
    {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->prix = $prix;
        $this->vitesse = $vitesse;
    }

    public function getMarque(): string
    {

        return $this->marque;
    }
    public function getCouleur(): string
    {

        return $this->couleur;
    }
    public function getPrix(): float
    {

        return $this->prix;
    }

    public function getVitesse(): float
    {

        return $this->vitesse;
    }

    public function setMarque($marque)
    {
        $this->marque = $marque;
    }

    public function setCouleur($couleur)
    {
        $this->couleur = $couleur;
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    public function setVitesse($vitesse)
    {
        $this->vitesse = $vitesse;
    }

    public function __toString()
    {
        return
            " Marque : " . $this->marque .
            " Couleur : " . $this->couleur .
            " Prix : " . $this->prix . "€" .
            " Vitesse : " . $this->vitesse . "Km";
    }

    public function avancer(){
        echo "Le vehicule avance";
    }
}
