<?php

class Elephant extends Animaux
{

    private $nbdefense;

    public function __construct($nbdefense, $poids, $hauteur, $longueur)
    {
        parent::__construct($poids, $hauteur, $longueur);
        $this->nbdefense = $nbdefense;
    }

    public function getNbdefense()
    {
        return $this->nbdefense;
    }

    public function setNbdefense()
    {
        $this->nbdefense;
    }



    public function  __toString()
    {
        return parent::__toString() . " nombre de defense : " . $this->nbdefense;
    }


    public function nager()
    {
        echo " L'elephants ne mage pas !! ";
    }

    public function manger()
    {
        echo " L'elephant mange des feuilles !!";
    }
}
