<?php

class Humain
{

    private $Firstname;
    private $Lastname;
    private $Email;
    private $Password;
    private $Adress;

    public function __construct($Firstname, $Lastname, $Email, $Password, $Adress)
    {

        $this->Firstname = $Firstname;
        $this->Lastname = $Lastname;
        $this->Email = $Email;
        $this->Password = $Password;
        $this->Adress = $Adress;
    }

    // first name
    public function getFirstname()
    {

        return ($this->Firstname);
    }

    public function setFirstname($newFirstname)
    {

        $this->Firstname = $newFirstname;
    }
    // last name
    public function getLastname()
    {

        return ($this->Lastname);
    }

    public function setLastname($newLastname)
    {

        $this->Lastname = $newLastname;
    }

    // email


    public function getEmail()
    {

        return ($this->Email);
    }

    public function setEmail($newEmail)
    {

        $this->Email = $newEmail;
    }
    // pasword

    public function getPassword()
    {

        return ($this->Password);
    }


    public function get($newPassword)
    {

        $this->Password = $newPassword;
    }
    // adress

    public function getAdress()
    {

        return ($this->Adress);
    }

    public function setAdress($newAdress)
    {

        $this->Adress = $newAdress;
    }



    public function __toString()
    {
        return "Cette personne s'apelle  " . $this->Firstname . " . son mon de famille est " . $this->Lastname . " , son email est " . $this->Email .
            " son mot de pass est " . $this->Password . " est son adresse est " . $this->Adress . "<br>";
    }

    public function speak($thing)
    {
        echo "Cette peronne dit : " . $thing;
    }
}
