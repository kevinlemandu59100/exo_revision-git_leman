<?php

// Création d'une classe abstraite EtreVivant
// mémo : une classe abstraite ne peut pas être instancier !
abstract class EtreVivant
{

    // $compteurEtreVivant est une variable de class (n'a pas de rapport avec l'objet )
    public static $compteurEtreVivant = 1 ;

    // l'opérateur private permet que l'attribut soit accessible que depuis la classe
    private $id;
    private $age;
    private $taille;
    
    
	// l'opérateur d'accessibilité protected permet que l'attribut 
	//  soit accessible depuis la classe et les classes enfants
    protected $nom;


    public function __construct( $age, $taille, $nom)
    {
        $this->id =  self::$compteurEtreVivant++ ;
        $this->age = $age;
        $this->taille = $taille;
        $this->nom = $nom;
    }


     //Methode __ magique ( Attention si l'attribut n'est pas présent !! )
    // public function __get($attribut)
    // {
    //     return  $this->$attribut;
    // }


    //Methode __set magique ( Attention si l'attribut n'est pas présent !! )
    public function __set($attribut, $value)
    {
        $this->$attribut  = $value;
    }

    public function __toString()
    {
        return
         " Id : "  . $this->id .
         " Age : "  . $this->age .
         " Nom : "  . $this->nom .
         " Taille : "  . $this->taille ;
    
    
    }

    public function manger(){
        echo "mange" ; 
    }

  // une méthode abstraitre est une méthode que l'on définie juste par sa signature,
// elle devra être obligatoirement définie dans les classe enfants et implémenter
    abstract function boire() ;
}
