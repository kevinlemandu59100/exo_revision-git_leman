<?php

// Exercice 1 :  Crée une fonction pour la division que retourne le résultat

// Exercice 2 :  Crée une fonction pour la multiplication que retourne le résultat

// Exercice 3 :  Crée une fonction qui affiche l'intégralité d'un tableau donné.

// Exercice 4 Créer une fonction from scratch qui s'appelle verificationPassword(). Elle prendra un parametre de type string. 
// Elle devra retourner un boolean qui vaut true si le password fait au moins 8 caractères et false si moins.

// Exercice 5 :  Crée une fonction tableMultiplcation() qui pemettra d'afficher le rendu suivant : 
// 1   2   3   4   5
// 2   4   6   8  10
// 3   6   9  12  15
// 4   8  12  16  20
// 5  10  15  20  25

// La limite pourra être donner en paramétre.
// exemple si l'on donne 6 en paramétre. 
// 1   2   3   4   5   6  
// 2   4   6   8  10   12  
// 3   6   9  12  15   18
// 4   8  12  16  20   24
// 5  10  15  20  25   30  

// Exercice 6 :  Crée une fonction qui affiche la table de multiplication  d'un chiffre, jusqu'a une valeur donnée.


// Exercice 7 : Crée une fonction qui retourne un tableau correspondant à l'ordre inverse des éléments d'un tableau sans utiliser de boucle ( fonction native php) .

// Exercice 8 : Créé une fonction calculatrice qui effectue le traitement à l'aide de fonction et retourne le résultats elle prendra en paramétre les nombres  ansi que l'opérateur. 


// exo 1
/** 
 * @param float nb1 le nombre que l'on veut diviser
 * @param float nb2 le nombre qui est le diviseur
 * @return mixed soi le resultat soit un string avec un message d'erreur
 */
function divission(float $nb1, float $nb2)
{
    if ($nb1 == 0 || $nb2 == 0) {
        return "pas de 0 pour la division";
    }

    $res = $nb1 / $nb2;
    return $res;
}

echo divission(10, 5);
echo "<hr>";

// exo 2

function multiplication($nb1, $nb2)
{


    $res = $nb1 * $nb2;
    return $res;
}

echo  multiplication(10, 5);

"<hr>";

// exo 3

function tableaux(array $tab)
{

    foreach ($tab as $value) {
        echo $value . "<br>";
    }
}
echo "<br>";
$tab = [5, 8, 4, 8];
tableaux($tab);
echo "<hr>";

// exo 4

function verificationPassword($password)
{
    $longueur = strlen($password);

    if ($longueur >= 8) {
        return true;
    } else {
        return false;
    }
}

$password = "123456789";

if (verificationPassword($password)) {
    echo "le mot de passse fait 8 caractères ou plus ";
} else {
    echo "le mot de passe fait moins de 8 caractères";
}


echo "<hr>";


//  exo 5

function tableMultiplication(int $nb)
{
    for ($i = 1; $i <= $nb; $i++) {

        for ($j = 1; $j <= $nb; $j++) {

            echo $i * $j . " ";
        }
        echo "<br>";
    }
}

tableMultiplication(5);

echo "<hr>";
// exo 6


function multi(int $nb)
{

    for ($i = 1; $i <= 10; $i++) {
        $total = $nb * $i;

        echo  $nb . " X " . $i . " = " . $total . "<br>";
    }
}

multi(10);
echo"<hr>";

// exo 7

function tableaux2 ( array $tab ){

$tabi = array_reverse($tab);
foreach ($tabi as $value) {
    echo $value . "<br>";
}

}

tableaux2([5, 8, 4, 8]) ;
echo "<hr>";
// exo 8

function soustraction($nombre1, $nombre2)
{
    return $nombre1 - $nombre2;
}

function addition($n1, $n2)
{

    $res =  $n1 + $n2;

    return $res;
}

function division($n1, $n2)
{

    $res =  $n1 / $n2;

    return $res;
}


function calculatrice($nombre1, $nombre2, $operateur)
{
    switch ($operateur) {
        case '+':
            $resultat = addition($nombre1, $nombre2);
            break;
        case '-':
            $resultat = soustraction($nombre1, $nombre2);
            break;
        case '*':
            $resultat = multiplication($nombre1, $nombre2);
            break;
        case '/':
            $resultat = division($nombre1, $nombre2);
            break;
        default:
            $resultat = "Je peux rien faire avec ça moi!";
    }
    return $resultat;
} 

echo calculatrice(5, 5, "+");

