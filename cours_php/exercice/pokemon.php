<?php


$listeDesPokemon = [

    'Pikachu' => [
        'Taille' => 0.4,
        'Poids' => 6,
    ],
    'Raichu' => [
        'Taille' => 0.8,
        'Poids' => 30,
    ],
    'Salameche' => [
        'Taille' => 1,
        'Poids' => 50,
    ],
    'Reptincel' => [
        'Taille' => 1.4,
        'Poids' => 60,
    ],
    'Dracaufeau' => [
        'Taille' => 2,
        'Poids' => 180,
    ],

];
?>

<ul><?php
    foreach ($listeDesPokemon as $pokemon => $physique) {
    ?> <li> <?php echo $pokemon ?> </li>

        <?php
        foreach ($physique as $taille => $poids) {
        ?> <ul>
                <li> <?php echo $taille . " : " . $poids; ?>
            </ul>
            </li>


    <?php
        }
    }

    ?>
</ul>

<?php

