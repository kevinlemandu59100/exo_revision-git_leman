<?php

class Oiseau extends Animal {

    private int $nbPlumes ;

  
    public function __construct( $nbPlumes , $id , $nom, $couleur , $poids)
    {
     parent::__construct($id, $nom, $couleur , $poids) ;
     $this->nbPlumes = $nbPlumes;
        
    }

    public function getNbPlumes( ){
        return $this->nbPlumes;
    }

    public function setNbPlumes($nbPlumes){
        $this->nbPlumes = $nbPlumes;
    }

    public function __toString()
    {
        return parent::__toString() .  " nombre plumes " . $this->nbPlumes;
    }

    public function voler(){
        echo "l'oiseau vole " ;
    }

}