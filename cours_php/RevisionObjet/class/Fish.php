<?php

class fish extends EtreVivant
{

    private $numberOfFin;

    public function __construct($numberOfFin, $age, $height, $name)
    {

        parent::__construct($age, $height, $name);
        $this->numberOfFin = $numberOfFin;
    }


    public function getNumberOfFin()
    {
        return $this->numberOfFin;
    }

    public function boire()
    {

        echo " you don't need to drink ";
    }

    public function __toString()
    {

        return parent::__toString() . " nombre de nageoire : " . $this->numberOfFin;
    }

    public function setNumberOfFin($numberOfFin)
    {
        $this->numberOfFin = $numberOfFin;
    }
}
