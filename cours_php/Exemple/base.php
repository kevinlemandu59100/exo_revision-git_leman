<?php

// echo c'est une fonction qui permet d'afficher sur notre document
echo ("Hello world !! ");


$maVariable = 5;

$b = 4;

echo $maVariable . " <br> ";
// on fait une concaténation
echo "la valeur de b est : " . $b;

// permet d'afficher les information sur l'interpréteur de php
// phpinfo();

$a = -123; // variable de typr Integer
$c = 1.28;
$b = false;
$d = "Bonjour ! ";

// camelCase est la convention de nommage qu'il faut en php pour les variables
$unPrenomAleatoire = "Toto";

$monTableau = array(1, 2, 3, 4);
$monDeuxiemeTableau = [1, 2, 3, 4, 5, 6];

var_dump($monDeuxiemeTableau);
echo "<br>";
var_dump($monTableau);


$prenoms = array(0 => "Toto", "salut" => "Tata");

echo $prenoms[0] . " " . $prenoms["salut"];

$prenoms[1] = "<br>Salut comment ca va ?";

echo $prenoms[1];

// structure conditionnelles

$jour = "Vendredi";

if ($jour == "lundi") {

    echo "c'est la reprise de la semaine";
} elseif ($jour == "vendredi") {

    echo "c'est bientot le Weenkend";
} else {

    echo " c'estl l'heure de la pause !!<br>";
}

// $a = 2;
// $b = 5;
// $c;

// $c = $b;
// $b = $a;
// $a = $c;

// echo "la valeur de a est de " . $a . " la valeur de b est de " . $b ;


// $age = 6;

// if ($age >= 6 && $age <= 7){
//     echo $age . " ans c'est Poussin";
// }


// elseif ($age >= 8 && $age <= 9){
// echo $age . " ans c'est Pupille";
// }
// elseif ($age >= 10 && $age <= 11){
// echo $age . " ans c'est Minime";
// }
// elseif ($age >= 12){
// echo $age . " ans c'est Cadet";
// }

// les incrémentations

$a = 2;
$b = 2;
$c = 2;
$d = 2;

echo "Post incrémentation pour a : " . $a++ . "<br> ";
echo " a contient maintenant : " . $a . "<br>";

echo "Pré incrémentation pour b : " . ++$b . "<br> ";
echo " b contient maintenant : " . $b . "<br>";

echo "Post décrémentation pour c : " . $c-- . "<br> ";
echo " c contient maintenant : " . $c . "<br>";

echo "Pré décrémentation pour d : " . --$d . "<br> ";
echo " d contient maintenant : " . $d . "<br>";




//les structures iteratives 

//La boucle PHP for (« pour » en français) 
// fonctionnement d’une boucle for selon trois phases :
//     Une phase d’initialisation ; $i = 0
//     Une phase de test ; $i <= 5
//     Une phase d’incrémentation. $i++
for ($i = 0; $i <= 5; $i++) {

    echo " la valeur de i est de : " . $i . "<br>";
}
//La boucle while  («tant que » en français) va nous permettre
//d’exécuter un certain bloc de code « tant qu’une » condition donnée est vérifiée.
$nb = 0;
while ($nb <= 10) {
    $nb++;
    echo " la valeur de nb : " . $nb . "<br>";
}

$x = 21;
$y = 20;

//La boucle PHP do… while (« faire… tant que » en français) 
//ressemble à priori à la boucle while mais effectuera toujours au moins un passage
do {

    $x++;
    echo $x . "<br>";
} while ($y > $x);
