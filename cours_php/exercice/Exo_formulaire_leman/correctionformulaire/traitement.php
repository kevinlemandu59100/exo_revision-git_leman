<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="UTF-8">
    <title>AFFICHAGE</title>
    <link rel="stylesheet" href="style.css">
  </head>
  
  <body>   
    <?php
    // Vérification si le formulaire a été soumis
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        // Récupération des valeurs des champs du formulaire
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $adresse = $_POST['adresse'];
        $ville = $_POST['ville'];
        $code_postal = $_POST['code_postal'];
        $telephone = $_POST['telephone'];
        $email = $_POST['email'];

        // Vérification si le fichier de sauvegarde existe
        $fichierExiste = file_exists("enregistrements.txt");

        // Ouverture du fichier de sauvegarde en mode ajout ou création
        $fichier = fopen("enregistrements.txt", "a");

        // Si le fichier n'existait pas, on écrit la première ligne de séparation
        if (!$fichierExiste) {
            fwrite($fichier, "---------------------------\n");
        }

        // Écriture de la date et de l'heure actuelles dans le fichier
        $dateHeure = date("Y-m-d H:i:s");
        fwrite($fichier, "Date et heure : " . $dateHeure . "\n");

        // Écriture des données du formulaire dans le fichier
        fwrite($fichier, "Nom : " . $nom . "\n");
        fwrite($fichier, "Prenom : " . $prenom . "\n");
        fwrite($fichier, "Adresse : " . $adresse . "\n");
        fwrite($fichier, "Ville : " . $ville . "\n");
        fwrite($fichier, "Code Postal : " . $code_postal . "\n");
        fwrite($fichier, "Telephone : " . $telephone . "\n");
        fwrite($fichier, "Email : " . $email . "\n");
        fwrite($fichier, "---------------------------\n");

        // Fermeture du fichier
        fclose($fichier);

        // Affichage des données
        echo "<h1>Donnees enregistrees :</h1>";
        echo "<table>";
        echo "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Telephone</th><th>Email</th></tr>";
        echo "<tr><td>$dateHeure</td><td>$nom</td><td>$prenom</td><td>$adresse</td><td>$ville</td><td>$code_postal</td><td>$telephone</td><td>$email</td></tr>";
        echo "</table>";

        // Affichage des données du fichier enregistrements.txt
        echo "<h1>Données du fichier enregistrements.txt :</h1>";
        // Lecture du contenu du fichier "enregistrements.txt"
$contenu = file_get_contents("enregistrements.txt");

// Séparation des enregistrements en utilisant la ligne de séparation
$enregistrements = explode("---------------------------\n", $contenu);

// Création du tableau HTML
$tableHTML = "<table>\n";
$tableHTML .= "<tr><th>Date et heure</th><th>Nom</th><th>Prénom</th><th>Adresse</th><th>Ville</th><th>Code Postal</th><th>Telephone</th><th>Email</th></tr>\n";

// Parcours des enregistrements
foreach ($enregistrements as $enregistrement) {
    // Suppression des caractères de nouvelle ligne et espaces en début et fin
    $enregistrement = trim($enregistrement);

    // Vérification si l'enregistrement est vide
    if (!empty($enregistrement)) {
        // Séparation des champs en utilisant les retours à la ligne
        $champs = explode("\n", $enregistrement);

        // Récupération des valeurs des champs en supprimant les étiquettes
        $dateHeure = substr($champs[0], 15);
        $nom = substr($champs[1], 6);
        $prenom = substr($champs[2], 9);
        $adresse = substr($champs[3], 10);
        $ville = substr($champs[4], 7);
        $codePostal = substr($champs[5], 14);
        $telephone = substr($champs[6], 12);
        $email = substr($champs[7], 8);

        // Vérification si le champ "Code Postal" est vide
        // Vérification si l'une des variables ($dateHeure, $nom , $prenom , $adresse, $ville, $codePostal, $telephone, $email) est vide
    if (empty($dateHeure) || empty($nom) || empty($prenom) || empty($adresse) || empty($ville) || empty($codePostal) || empty($telephone) || empty($email)) {
        // Attribuer la valeur "N/A" à toutes les variables vides
        if (empty($dateHeure)) {
            $dateHeure = "N/A";
        }
        if (empty($nom)) {
            $nom = "N/A";
        }
        if (empty($prenom)) {
            $prenom = "N/A";
        }
        if (empty($adresse)) {
            $adresse = "N/A";
        }
        if (empty($ville)) {
            $ville = "N/A";
        }
        if (empty($codePostal)) {
            $codePostal = "N/A";
        }
        if (empty($telephone)) {
            $telephone = "N/A";
        }
        if (empty($email)) {
            $email = "N/A";
        }   
    }

        // Ajout d'une ligne au tableau HTML avec les valeurs des champs
        $tableHTML .= "<tr><td>$dateHeure</td><td>$nom</td><td>$prenom</td><td>$adresse</td><td>$ville</td><td>$codePostal</td><td>$telephone</td><td>$email</td></tr>\n";
    }
}

// Fermeture du tableau HTML
$tableHTML .= "</table>\n";

// Affichage du tableau HTML
echo $tableHTML;
    }
    ?>
</body>
</html>
       