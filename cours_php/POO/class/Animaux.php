<?php

class Animaux
{
   // attributs

   private $espece;
   private $age;
   private $lieu;
   private $alimentation;
   private $image;

   // constructeur (usine)
   public function __construct($espece, $age, $lieu, $alimentation, $image)
   {


      // object construit 
      $this->espece = $espece;
      $this->setAge($age);
      $this->lieu = $lieu;
      $this->setalimentation($alimentation);
      $this->image = $image;
   }

   // accesseur / = getter
   // espece
   public function getNom()
   {
      return strtoupper($this->espece);
   }

   // mutateur / = settter
   public function setNom($nouveauNom)
   {

      $this->espece = $nouveauNom;
   }

   // lieu
   public function getLieu()
   {

      return $this->lieu;
   }
   public function setLieu($lieu)
   {

      $this->lieu = $lieu;
   }

   // age

   public function getAge()
   {

      return $this->age . " ans";
   }

   public function setage($nouvelAge)
   {

      if ($nouvelAge >= 1 && $nouvelAge  <= 100) {
         $this->age = $nouvelAge;
      }
   }


   // alimentation

   public function getalimentation()
   {
      return $this->alimentation;
   }
   public function setalimentation($alimentation)
   {
      if (is_string($alimentation)) {
         $this->alimentation = $alimentation;
      }
   }

   //   image

   public function getImage()
   {
      return '<img src="' . $this->image . '">';
   }
   public function setimage($image)
   {

      $this->image = $image;
   }

   public function __toString()
   {
      return "L'animal est  " . $this->espece . " . Il a " . $this->getAge() . " , il habite dans la " . $this->lieu .
         " il mange des " . $this->alimentation . " . " . $this->getImage() . "<br>";
   }
}
