<?php

class Lion extends Animaux
{

    private $heuredesomeil;

    public function __construct($heuredesomeil, $poids, $hauteur, $longueur)
    {
        parent::__construct($poids, $hauteur, $longueur);
        $this->heuredesomeil = $heuredesomeil;
    }

    public function getHeuredesomeil()
    {
        return $this->heuredesomeil;
    }

    public function setHeuresdesomeil()
    {
        $this->heuredesomeil;
    }



    public function  __toString()
    {
        return parent::__toString() . " Heures des someil : " . $this->heuredesomeil . " heures .";
    }


    public function nager()
    {
        echo " Le lion ne mage pas !!";
    }

    public function manger()
    {
        echo " Le lion mange des antilopes !!";
    }
}
