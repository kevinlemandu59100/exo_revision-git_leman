<?php

class Crocodile extends Animaux
{

    private $amplitudegueul;

    public function __construct($amplitudegueul, $poids, $hauteur, $longueur)
    {
        parent::__construct($poids, $hauteur, $longueur);
        $this->amplitudegueul = $amplitudegueul;
    }

    public function getAmplitudegueul()
    {
        return $this->amplitudegueul;
    }

    public function setAmplitudegueul()
    {
        $this->amplitudegueul;
    }



    public function  __toString()
    {
        return parent::__toString() . " L'amplitude de la guele : " . $this->amplitudegueul . "cm .";
    }


    public function nager()
    {
        echo " Le crocodile mage !!";
    }

    public function manger()
    {
        echo " Le crocodile mange des gnous !!";
    }
}
