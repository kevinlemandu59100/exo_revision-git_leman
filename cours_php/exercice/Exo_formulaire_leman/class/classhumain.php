<?php


class Humain
{

    private $Firstname;
    private $Lastname;
    private $Email;
    private $rue;
    private $numerorue;
    private $ville;
    private $codepostale;
    private $numerotel;

    public function __construct($Firstname, $Lastname, $Email, $rue, $ville, $codepostale, $numerorue, $numerotel)
    {

        $this->Firstname = $Firstname;
        $this->Lastname = $Lastname;
        $this->Email = $Email;
        $this->rue = $rue;
        $this->ville = $ville;
        $this->codepostale = $codepostale;
        $this->numerorue = $numerorue;
        $this->numerotel = $numerotel;
    }

    // first name
    public function getFirstname()
    {

        return ($this->Firstname);
    }

    public function setFirstname($newFirstname)
    {

        $this->Firstname = $newFirstname;
    }
    // last name
    public function getLastname()
    {

        return ($this->Lastname);
    }

    public function setLastname($newLastname)
    {

        $this->Lastname = $newLastname;
    }

    // email


    public function getEmail()
    {

        return ($this->Email);
    }

    public function setEmail($newEmail)
    {

        $this->Email = $newEmail;
    }
    // rue

    public function getRue()
    {

        return ($this->rue);
    }


    public function get($newrue)
    {

        $this->Password = $newrue;
    }
    // ville

    public function getVille()
    {

        return ($this->ville);
    }

    public function setVille($newville)
    {

        $this->ville = $newville;
    }

    // code postale

    public function getCodepostale()
    {

        return ($this->codepostale);
    }

    public function setCodepostale($newcodepostale)
    {

        $this->codepostale = $newcodepostale;
    }

    // numero de rue

    public function getNumerorue()
    {

        return ($this->numerorue);
    }

    public function setNumerorue($newnumerorue)
    {

        $this->numerorue = $newnumerorue;
    }

    // numero de telephone

    public function getNumerotel()
    {

        return ($this->numerotel);
    }

    public function setNumerotel($newnumerotel)
    {

        $this->numerotel = $newnumerotel;
    }


    public function __toString()
    {
        return "votre nom  " . $this->Firstname . " . votre mon de famille est " . $this->Lastname . " , votre email est " . $this->Email .
            " votre adresse est " . $this->numerorue . $this->rue . " votre code postale est " . $this->codepostale . " votre ville est " . $this->ville .
            " votre téléphone est " . $this->numerotel . ".<br>";
    }
}
