<?php

abstract class  Animaux
{

    private $poids;
    private $hauteur;
    private $longueur;

    public function __construct($poids, $hauteur, $longueur,)
    {

        $this->poids = $poids;
        $this->hauteur = $hauteur;
        $this->longueur = $longueur;
    }

    // poids
    public function getPoids()
    {
        return $this->poids;
    }

    public function setPoids($nouveaupoids)
    {

        $this->poids = $nouveaupoids;
    }

    // hauteur
    public function getHauteur()
    {

        return $this->hauteur;
    }
    public function setHauteur($nouveauhauteur)
    {

        $this->hauteur = $nouveauhauteur;
    }

    // longueur

    public function getlongueur()
    {

        return $this->longueur;
    }

    public function setlongueur($nouveaulongueur)
    {
        $this->longueur = $nouveaulongueur;
    }

    public function marcher()
    {

        echo " L'animal marche !";
    }

    public function __toString()
    {
        return "L'animal pese  " . $this->poids . " kilo" . " . Il messure " . $this->longueur . " métre de longueur , est " . $this->hauteur . " métre de haut .";
    }

    abstract public function nager();

    abstract public function manger();
}
