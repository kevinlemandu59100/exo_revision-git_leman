<?php 

abstract class CompteBancaire {

    protected $nom;
    protected $argent;

    
    public function __construct( $nom, $argent)
    {
        $this->nom = $nom;
        $this->argent = $argent;
 
    }

    public function getNom(){

        return $this->nom;

    }

    public function setNom( $nom ) {
        $this->nom = $nom;
    }

    public function getArgent(){

        return $this->argent;

    }

    public function setArgent( $argent ) {
        $this->argent = $argent;
    }

    public function __toString()
    {
        return
         "   "  . $this->nom .
         ": Solde : "  . $this->argent ;
    
    }

    abstract function debiter() ;
    abstract function crediter() ;

}