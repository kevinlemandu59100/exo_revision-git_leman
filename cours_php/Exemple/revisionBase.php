<!-- Pour ecrire du code php
 on doit le faire a l'interieure d'un Tag <?php // CODE 
                                            ?> -->

<?php

echo ' les type de données et les variable : ' . "<hr>";

// Déclaration de la variable maVariable dans laquel on affecte la valeur 5 . ( de type entier ( integer ))
// on utilise le $ pour faire appel a une variable en php
$mavariable = 5;

// ici le type de données est une chaine de carastères ( string )
$string = ' Bonjour ! ';

// ici le type données est un réel ( Float )
$decimal = 5.2;


// ici le typre de données est un boolean ( vrai ou faux )
$boolean = true;
$boolean = $mavariable < 0;


// var_dump nous permet d'afficher touts les informaions d'une variable
var_dump($boolean);

// echo est une fonction qui permet d'afficher quelques choses
echo $string;
echo "<hr>";
// Création d'un tableau stocker dans la variable $montableau
$monTableau = [22, 15, 95, 5.5];

var_dump($monTableau);

// affiche la valeur a la premiere case du tableau ( ici la case 0 ), donc 22
echo $monTableau[0];

// cration d'un tableau avec des clées des valeurs
$TableauAssociatif = ["premiereCle" => "Un", "deuxiemeCle" => "Deux"];

// ici la clée "premiereCle" renvoit la valeur " Un "
echo  " " . $TableauAssociatif["premiereCle"];


// La concatenation c'est mettre bout a bout des chaines de caractères en les "additionnant" avec le " . "

echo "<hr>";
echo $string . " Tout le monde !! ";

// Les sctructures condionelles ( les if...)


$age = rand(1, 99);

echo $age . "<br>";


if ($age < 10) {

    echo "C'est un enfant ! ";
} elseif ($age < 18) {

    echo "c'est un ados ! ";
} elseif ($age < 64) {

    echo "C'est un adulte";
} else {

    echo "C'est bientot la fin...";
}

// structure avec les switch
echo "<hr>";
$heure = rand(1, 12);

switch ($heure) {

    case 1:
        echo "il est 1h";
        break;
    case 2;
        echo "il est 2h";
        break;
    case 3:
        echo "il est 3h";
    case 4:
        echo "il est 4h";
        break;

    default:
        echo "il est plus de 4h...fonctionnaire... ";
}


echo "<hr>";

$nombre = rand(-100, 100);

echo $nombre;

if ($nombre > 0) {
    echo "C'est possitif !";
} else {
    echo "C'est negative ou zero !";
}

echo '<br>';

// Condition terenaire permet de remplacer une structure if...else
echo $nombre > 0 ? " <h1> positif </h1> "  : " <h1> negatif </h1> ";


// Les structure itératives

// $i++ c'est egal i = i + 1 ;

// La boucle PHP for (<< pour>> en francais)
// fonctionnement d'une boucle for selen trois phases :
for ($i = 0; $i < 10; $i++) {
    echo "la valeur de i est : " . $i . '<br>';
}


$nb = 0;

while ($nb != 5) {
    $nb = rand(1, 99);
    echo $nb . "<br>";
}


// Parcourir des tableaux avec boucles ( Strucutres itératives ) :


$monTableau = [1, 55, 8, 9];

// on créer une variable $tailleTableau
// dans laquel on affecte la valeur retourné par la fonction count( )
// qui permet de compter le nombre de valeur d'un tableau .

echo "<hr> affichage d'un tableau a l'aide d'une boucle for <br> ";
$tailleTableau = count($monTableau);

for ($i = 0; $i < $tailleTableau; $i++) {

    echo $monTableau[$i] . "<br>";
}

echo "<hr> affichage d'un tableau a l'aide d'une boucle foreach ( pour chaque ) <br> ";
foreach ($monTableau as $valeur) {

    echo $valeur . '<br>';
}


?>
<hr>
<p>Recupération d'une valeur d'un formulaire grace au tableau <strong>$_GET</strong>
<p>

    <?php

    $nom = $_GET["nom"];

    ?>


<p> Bonjour <?php echo $nom ?> ! </p>

<hr>

<?php

// une fonction c'est un bloque de code qui comporte un nom qui effectie une action ou plusieurs 
// qui peut retransmettre une valeur , et que l'on peu réutiliser


// signature de la fonction
// un parametre est iune variable temporaire qui n'est présente que dans la fonction
// il correspond a un inconnu, une valeur que l'on aura besoin dans la fonction


function addition($n1, $n2)
{

    $res = $n1 + $n2;

    return $res;
}


$resultatAddition = addition(5, 5);

echo $resultatAddition;


echo "<hr>";


// Exercice 1: Créer une fonction  qui s'appelle capital(). Elle prendra un parametre de type string. Elle devra retourner le nom de la capitale des pays suivants :
// France ==> Paris
// Allemagne ==> Berlin
// Italie ==> Rome
// Espagne ==> Madrid
// Angleterre ==> Londres
// Tout autre pays ==> Inconnu
// Il faudra utiliser la structure SWITCH pour faire cette exercice.


function capital($capital)
{

    switch ($capital) {
        case "France":
            echo "La capital de la France est Paris"; //ou return "Paris";
            break;
        case "Allemangne":
            echo "La capital de L'Allemagne est Berlin"; //ou return "Berlin";
            break;
        case "Italie":
            echo "La capital de L'Italie est Rome"; //ou return "Rome";
            break;
        case "Espagne":
            echo "La capital de L'Espagne est Madrid"; //ou return "Madrid";
            break;
        case "Angleterre":
            echo "La capital de L'Angleterre est Londres"; //ou return "Londres";
            break;
        case "Belgique":
            echo "La capital de la Belgique est Bruxelles"; //ou return "Bruxelles";
            break;
        case "Autriche":
            echo "La capital de L'Autriche est Vienne"; //ou return "Vienne";
            break;
        case "Danemark":
            echo "La capital du Danemark est Copenhague"; //ou return "Copenhague";
            break;
        case "Irlande":
            echo "La capital de L'Irlande est Dublin"; //ou return "Dublin";
            break;
        default;
            echo "La capital est inconnu";
    }
}
capital("Danemark");

// ou
// $ville = capital("France");
// echo 'la capital est  : ' . $ville ;

echo "<hr>";

// on peut donner une valeur par defaut a un parametre en lui affectant une valeur entre les ( )
function bonjourAvecValeurParDefaut($prenom = "Monsieur X ")
{

    echo "Bonjour " . $prenom . ".<br>";
}

bonjourAvecValeurParDefaut("kevin");
bonjourAvecValeurParDefaut();

function bonjourAvecUnNombreDeParametreVariables(...$prenoms)
{

    foreach ($prenoms as $personne) {
        echo 'bonjour ' . $personne . '<br>';
    }
}

bonjourAvecUnNombreDeParametreVariables("toto", "tata", "truc", "bidule");

// On peut donner un type pour chaque parametres en ecrivant le type de donnée devant le paramétre
/**
 * @param $n1 de type float correspond au prémier nombre que l'on veut additionner
 *@return float valeur de l'addition de $n1 est $n2
 */
function additionAvecTypage(float $n1, float  $n2) : float {
    return $n1 + $n2;
}

echo additionAvecTypage(5, 5);
echo"<hr>";




?> 