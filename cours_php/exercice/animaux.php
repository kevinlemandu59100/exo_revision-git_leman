<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Animaux</title>
    <link rel="stylesheet" href="animaux.css">
</head>

<body>

    <?php


    $animaux = [

        'Chat' => [
            'Longevité' => "15 Ans",
            'Couleur' => "Rouge",
            'Lieu' => "Maison",
            'Alimentation' => ['Croquette', 'Souris'],
            "Image" => "R.gif"
        ],

        'Chien' => [
            'Longevité' => "100 Ans",
            'Couleur' => "Bleu",
            'Lieu' => "Sur le canapé",
            'Alimentation' => ['Croquette', 'Viande'],
            "Image" => "R2.gif"
        ],

        'Panda' => [
            'Longevité' => "400 Ans",
            'Couleur' => "Blanc",
            'Lieu' => "Foret",
            'Alimentation' => ['Feuille', 'Banbou'],
            "Image" => "R4.gif"
        ],


        'Lion' => [
            'Longevité' => "2000 Ans",
            'Couleur' => "Marron",
            'Lieu' => "Chez moi !",
            'Alimentation' => ['Humain', 'Biche'],
            "Image" => "R3.gif"
        ],


        'Tigre blanc' => [
            'Longevité' => "50 Ans",
            'Couleur' => "Ben blanc ",
            'Lieu' => "Neige",
            'Alimentation' => ['Lapin', 'Phoque'],
            "Image" => "R5.gif"
        ],
    ];



    ?>

    <table border="1">
        <tr>
            <th>Nom </th>
            <th>Longévité</th>
            <th> Couleur </th>
            <th>Lieu</th>
            <th>Alimentation</th>
            <th>Image</th>

        </tr>

        <!-- Code php avec un foreach -->

        <?php foreach ($animaux as $animal => $donnee) : ?>

            <tr>

                <td><?php echo $animal ?></td>

                <?php foreach ($donnee as $key => $value) : ?>

                    <?php if ($key == "Alimentation") :  ?>

                        <td>
                            <ul>
                                <?php
                                foreach ($value as $aliment) :
                                ?>

                                    <li><?php echo $aliment  ?></li>
                                <?php endforeach ?>

                                <?php

                                ?>
                            </ul>
                        </td>

                    <?php elseif ($key == "Image") : ?>

                        <td><img src=" <?php echo $value  ?> " width="200px" height="150px"></td>

                    <?php else : ?>

                        <td><?php echo $value ?></td>

                    <?php endif  ?>
                    <?php   ?>
                <?php endforeach ?>
            </tr>

        <?php endforeach ?>



    </table>
</body>

</html>