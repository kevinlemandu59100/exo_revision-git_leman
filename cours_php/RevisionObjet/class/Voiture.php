<?php

require "Vehicule.php";
// la classe Voiture hérite de la classe Vehicule 
// l'héritage est marqué par le mot clé extends
class Voiture extends Vehicule  {

    private $nbPortes;
    private $nbCh; 


    public function __construct($nbPortes , $nbCh , $marque , $prix , $vitesse , $couleur )
    {

         //parent::__construct permet de faire appel  à la méthode __construct de la classe parent ( Vehicule )
        parent::__construct($marque, $couleur,$prix, $vitesse);
        $this->nbPortes = $nbPortes;
        $this->nbCh = $nbCh ;
    }

    public function getNbPortes(){
        return $this->nbPortes ;
    }
 
    public function getNbCh(){
        return $this->nbCh ;
    }


    public function __toString()
    {

         //parent::__toString permet de faire appel  à la méthode __toString de la classe parent ( Vehicule )
        return parent::__toString() 
         . "  Nombre de porte : " . $this->nbPortes 
         . "  Puissance : " . $this->nbCh ;
    }




}